import 'package:flutter/cupertino.dart';


//file constant to be accessible in many classes
const String STATUS_LOGIN = "STATUS_LOGIN";
const String TOKEN = "TOKEN";
const String USER_ID = "USER_ID";
const String IS_USER = "IS_USER";
const String PHONE_NUMBER = "PHONE_NUMBER";
const String EMAIL = "EMAIL";
const String NAME = "NAME";
const String BASE_URL = "https://zoomat.bayunugroho404.dev/api";
// const String BASE_URL = "http://192.168.1.19:8000/api";
const Color  secondary= Color(0xfffad49a);
const Color primary = Color(0xffffc56c);