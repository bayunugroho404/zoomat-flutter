

//modes restaurant ,this file to accommodate temporary data
class RestaurantModel{
  String? name ;
  String? image ;
  int? id;
  String? address;
  double? meter;
  double? lat;
  double? lng;
  String? no;

  RestaurantModel({this.name,this.meter,this.address, this.image, this.id, this.lat, this.lng,this.no});
}