import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'package:zoomat/app/data/remote/response/response_post_data.dart';
import 'package:zoomat/utils/constant.dart';

//this file for the API endpoint configuration

class ApiServiceImage {
  Future<ResponsePostData> addPhoto(
      String token, PickedFile image, String resto_id) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request =
        http.MultipartRequest("POST", Uri.parse("$BASE_URL/add_photo"));
//add text fields
    request.headers.addAll(headers);
    request.fields["resto_id"] = resto_id;
//create multipart using filepath, string or bytes
    var pic = await http.MultipartFile.fromPath("name", image.path);
//add multipart to request
    request.files.add(pic);
    var response = await request.send();
//Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    if (response.statusCode == 200) {
      return ResponsePostData.fromJson(json.decode(responseString));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponsePostData> addMenu(
      String token, PickedFile image, String resto_id) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request =
        http.MultipartRequest("POST", Uri.parse("$BASE_URL/add_menu"));
    request.headers.addAll(headers);
    request.fields["resto_id"] = resto_id;
    var pic = await http.MultipartFile.fromPath("name", image.path);
    request.files.add(pic);
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    if (response.statusCode == 200) {
      return ResponsePostData.fromJson(json.decode(responseString));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponsePostData> addResto(
    String token,
    PickedFile image,
    String name,
    String address,
    String lattitude,
    String longtitude,
    String timings,
    String cost,
    String vehicle,
    String phone,
    String category_id,
  ) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    var request =
        http.MultipartRequest("POST", Uri.parse("$BASE_URL/post_resto"));
    request.headers.addAll(headers);
    request.fields["name"] = name;
    request.fields["address"] = address;
    request.fields["lattitude"] = lattitude;
    request.fields["longtitude"] = longtitude;
    request.fields["timings"] = timings;
    request.fields["cost"] = cost;
    request.fields["vehicle"] = vehicle;
    request.fields["desc"] = phone;
    request.fields["category_id"] = category_id;
    var pic = await http.MultipartFile.fromPath("image", image.path);
    request.files.add(pic);
    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    if (response.statusCode == 200) {
      return ResponsePostData.fromJson(json.decode(responseString));
    } else {
      throw Exception('gagal');
    }
  }
}
