import 'package:http/http.dart' show Client, Response;
import 'package:zoomat/app/data/remote/response/response_get_all_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_category.dart';
import 'package:zoomat/app/data/remote/response/response_get_info_by_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_menu_by_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_photo_by_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_review_by_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_review_by_user.dart';
import 'package:zoomat/app/data/remote/response/response_login.dart';
import 'package:zoomat/app/data/remote/response/response_message.dart';
import 'package:zoomat/app/data/remote/response/response_post_data.dart';
import 'package:zoomat/app/data/remote/response/response_post_review.dart';
import 'package:zoomat/app/data/remote/response/response_status.dart';
import 'dart:convert';

import 'package:zoomat/utils/constant.dart';

//this file for the API endpoint configuration
class APIService {
  Client client = Client();
  String base_url = BASE_URL;

  Future<ResponseLogin> postLogin(String email, String password) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/auth/login"), body: {
      "email": email,
      "password": password,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseLogin.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetAllRestoByCategory> getAllRestaurantByCategory(String category_id) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/get_resto_by_category"), headers: {
    },body: {
      "category_id": category_id
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetAllRestoByCategory.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseStatus> updateVehicle(String token,String id,String text) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/update_vehicle"), headers: {
      "Authorization": 'Bearer $token',
    },body: {
      "id": id,
      "vehicle": text,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseStatus.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseMessage> updateCost(String token,String id,String text) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/update_cost"), headers: {
      "Authorization": 'Bearer $token',
    },body: {
      "id": id,
      "cost": text,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseMessage> remove_menu(String token,String id) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/delete_menu"), headers: {
      "Authorization": 'Bearer $token',
    },body: {
      "id": id,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseMessage> remove_photo(String token,String id) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/delete_photo"), headers: {
      "Authorization": 'Bearer $token',
    },body: {
      "id": id,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseMessage> remove_resto(String token,String id) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/delete_resto"), headers: {
      "Authorization": 'Bearer $token',
    },body: {
      "id": id,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetCategory> getCategory() async {
    Response response;
    response = await client.get(Uri.parse("$base_url/category"));
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetCategory.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }


  Future<ResponseGetMenuByResto> getMenuByResto( String id) async {
    Response response;
    response =
        await client.post(Uri.parse("$base_url/get_menu_by_id"), headers: {
    }, body: {
      "id": id
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetMenuByResto.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetInfoByResto> getInfoByResto( String id) async {
    Response response;
    response =
        await client.post(Uri.parse("$base_url/get_info_by_id"), headers: {
      // "Authorization": 'Bearer $token',
    }, body: {
      "id": id
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetInfoByResto.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetPhotoByResto> getPhotoByResto(
       String id) async {
    Response response;
    response =
        await client.post(Uri.parse("$base_url/get_photo_by_id"), headers: {
      // "Authorization": 'Bearer $token',
    }, body: {
      "id": id
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetPhotoByResto.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

 Future<ResponseMessage> updateProfile(
      String token,
     String name,
     String phone_number,
     String email,
     ) async {
    Response response;
    response =
        await client.post(Uri.parse("$base_url/user_update"), headers: {
      "Authorization": 'Bearer $token',
    }, body: {
      "name": name,
      "phone_number": phone_number,
      "email": email,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponsePostReview> postReview(
    String token,
    String rate,
    String user_id,
    String name,
    String resto_id,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/add_review"), headers: {
      "Authorization": 'Bearer $token',
    }, body: {
      "rate": rate,
      "user_id": user_id,
      "name": name,
      "resto_id": resto_id,
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponsePostReview.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetRiviewByUser> getReviewByUser(
      String token, String id) async {
    Response response;
    response =
        await client.post(Uri.parse("$base_url/get_review_by_user"), headers: {
      "Authorization": 'Bearer $token',
    }, body: {
      "id": id
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetRiviewByUser.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseGetReviewByResto> getReviewByResto(
       String id) async {
    Response response;
    response =
        await client.post(Uri.parse("$base_url/get_review_by_id"), headers: {
      // "Authorization": 'Bearer $token',
    }, body: {
      "id": id
    });
    print(response.body);
    if (response.statusCode == 200) {
      return ResponseGetReviewByResto.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseMessage> postRegister(
    String name,
    String phone_number,
    String email,
    String password,
    String password_confirmation,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/auth/register"), body: {
      "name": name,
      "phone_number": phone_number,
      "email": email,
      "is_user": "1",
      "password": password,
      "password_confirmation": password_confirmation,
    });
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }

  Future<ResponseMessage> postInfo(
    String name,
    String resto_id,
    String token,
  ) async {
    Response response;
    response = await client.post(Uri.parse("$base_url/add_info"), body: {
      "name": name,
      "resto_id": resto_id,
    }, headers: {
      "Authorization": 'Bearer $token',
    });
    if (response.statusCode == 200) {
      return ResponseMessage.fromJson(json.decode(response.body));
    } else {
      throw Exception('gagal');
    }
  }
}
