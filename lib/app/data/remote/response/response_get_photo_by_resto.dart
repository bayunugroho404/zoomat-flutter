import 'dart:convert';

ResponseGetPhotoByResto responseGetPhotoByRestoFromJson(String str) => ResponseGetPhotoByResto.fromJson(json.decode(str));

String responseGetPhotoByRestoToJson(ResponseGetPhotoByResto data) => json.encode(data.toJson());

class ResponseGetPhotoByResto {
  ResponseGetPhotoByResto({
    this.statusCode,
    this.resto,
    this.photos,
  });

  int? statusCode;
  Resto? resto;
  List<Photo>? photos;

  factory ResponseGetPhotoByResto.fromJson(Map<String, dynamic> json) => ResponseGetPhotoByResto(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    resto: json["resto"] == null ? null : Resto.fromJson(json["resto"]),
    photos: json["photos"] == null ? null : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "resto": resto == null ? null : resto?.toJson(),
    "photos": photos == null ? null : List<dynamic>.from(photos!.map((x) => x.toJson())),
  };
}

class Photo {
  Photo({
    this.id,
    this.name,
    this.restoId,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  int? restoId;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    restoId: json["resto_id"] == null ? null : json["resto_id"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "resto_id": restoId == null ? null : restoId,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}

class Resto {
  Resto({
    this.id,
    this.name,
    this.address,
    this.lattitude,
    this.image,
    this.longtitude,
    this.timings,
    this.cost,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? address;
  String? lattitude;
  String? image;
  String? longtitude;
  String? timings;
  String? cost;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Resto.fromJson(Map<String, dynamic> json) => Resto(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    address: json["address"] == null ? null : json["address"],
    lattitude: json["lattitude"] == null ? null : json["lattitude"],
    image: json["image"] == null ? null : json["image"],
    longtitude: json["longtitude"] == null ? null : json["longtitude"],
    timings: json["timings"] == null ? null : json["timings"],
    cost: json["cost"] == null ? null : json["cost"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "address": address == null ? null : address,
    "lattitude": lattitude == null ? null : lattitude,
    "image": image == null ? null : image,
    "longtitude": longtitude == null ? null : longtitude,
    "timings": timings == null ? null : timings,
    "cost": cost == null ? null : cost,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
