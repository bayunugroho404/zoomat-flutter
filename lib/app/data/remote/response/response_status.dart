
import 'dart:convert';

ResponseStatus responseStatusFromJson(String str) => ResponseStatus.fromJson(json.decode(str));

String responseStatusToJson(ResponseStatus data) => json.encode(data.toJson());

class ResponseStatus {
  ResponseStatus({
    this.status,
  });

  String? status;

  factory ResponseStatus.fromJson(Map<String, dynamic> json) => ResponseStatus(
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
  };
}
