
import 'dart:convert';

ResponseGetCategory responseGetPhotoByPhotofFromJson(String str) => ResponseGetCategory.fromJson(json.decode(str));

String responseGetPhotoByPhotofToJson(ResponseGetCategory data) => json.encode(data.toJson());

class ResponseGetCategory {
  ResponseGetCategory({
    this.statusCode,
    this.resto,
  });

  int? statusCode;
  List<ResponseGetCategoryResto>? resto;

  factory ResponseGetCategory.fromJson(Map<String, dynamic> json) => ResponseGetCategory(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    resto: json["resto"] == null ? null : List<ResponseGetCategoryResto>.from(json["resto"].map((x) => ResponseGetCategoryResto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "resto": resto == null ? null : List<dynamic>.from(resto!.map((x) => x.toJson())),
  };
}

class ResponseGetCategoryResto {
  ResponseGetCategoryResto({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.restos,
  });

  int? id;
  String? name;
  DateTime? createdAt;
  DateTime? updatedAt;
  List<RestoResto>? restos;

  factory ResponseGetCategoryResto.fromJson(Map<String, dynamic> json) => ResponseGetCategoryResto(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    restos: json["restos"] == null ? null : List<RestoResto>.from(json["restos"].map((x) => RestoResto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "restos": restos == null ? null : List<dynamic>.from(restos!.map((x) => x.toJson())),
  };
}

class RestoResto {
  RestoResto({
    this.id,
    this.name,
    this.address,
    this.vehicle,
    this.lattitude,
    this.image,
    this.longtitude,
    this.timings,
    this.categoryId,
    this.cost,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? address;
  String? vehicle;
  String? lattitude;
  String? image;
  String? longtitude;
  String? timings;
  int? categoryId;
  String? cost;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory RestoResto.fromJson(Map<String, dynamic> json) => RestoResto(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    address: json["address"] == null ? null : json["address"],
    vehicle: json["vehicle"] == null ? null : json["vehicle"],
    lattitude: json["lattitude"] == null ? null : json["lattitude"],
    image: json["image"] == null ? null : json["image"],
    longtitude: json["longtitude"] == null ? null : json["longtitude"],
    timings: json["timings"] == null ? null : json["timings"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    cost: json["cost"] == null ? null : json["cost"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "address": address == null ? null : address,
    "vehicle": vehicle == null ? null : vehicle,
    "lattitude": lattitude == null ? null : lattitude,
    "image": image == null ? null : image,
    "longtitude": longtitude == null ? null : longtitude,
    "timings": timings == null ? null : timings,
    "category_id": categoryId == null ? null : categoryId,
    "cost": cost == null ? null : cost,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
