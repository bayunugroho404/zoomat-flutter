// To parse this JSON data, do
//
//     final responseGetAllRestoByCategory = responseGetAllRestoByCategoryFromJson(jsonString);

import 'dart:convert';

ResponseGetAllRestoByCategory responseGetAllRestoByCategoryFromJson(String str) => ResponseGetAllRestoByCategory.fromJson(json.decode(str));

String responseGetAllRestoByCategoryToJson(ResponseGetAllRestoByCategory data) => json.encode(data.toJson());

class ResponseGetAllRestoByCategory {
  ResponseGetAllRestoByCategory({
    this.statusCode,
    this.resto,
  });

  int? statusCode;
  List<Resto>? resto;

  factory ResponseGetAllRestoByCategory.fromJson(Map<String, dynamic> json) => ResponseGetAllRestoByCategory(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    resto: json["resto"] == null ? null : List<Resto>.from(json["resto"].map((x) => Resto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "resto": resto == null ? null : List<dynamic>.from(resto!.map((x) => x.toJson())),
  };
}

class Resto {
  Resto({
    this.id,
    this.name,
    this.address,
    this.vehicle,
    this.lattitude,
    this.image,
    this.longtitude,
    this.timings,
    this.desc,
    this.categoryId,
    this.cost,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? address;
  String? vehicle;
  String? lattitude;
  String? image;
  String? longtitude;
  String? timings;
  String? desc;
  int? categoryId;
  String? cost;
  dynamic? deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Resto.fromJson(Map<String, dynamic> json) => Resto(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    address: json["address"] == null ? null : json["address"],
    vehicle: json["vehicle"] == null ? null : json["vehicle"],
    lattitude: json["lattitude"] == null ? null : json["lattitude"],
    image: json["image"] == null ? null : json["image"],
    longtitude: json["longtitude"] == null ? null : json["longtitude"],
    timings: json["timings"] == null ? null : json["timings"],
    desc: json["desc"] == null ? null : json["desc"],
    categoryId: json["category_id"] == null ? null : json["category_id"],
    cost: json["cost"] == null ? null : json["cost"],
    deletedAt: json["deleted_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "address": address == null ? null : address,
    "vehicle": vehicle == null ? null : vehicle,
    "lattitude": lattitude == null ? null : lattitude,
    "image": image == null ? null : image,
    "longtitude": longtitude == null ? null : longtitude,
    "timings": timings == null ? null : timings,
    "desc": desc == null ? null : desc,
    "category_id": categoryId == null ? null : categoryId,
    "cost": cost == null ? null : cost,
    "deleted_at": deletedAt,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
