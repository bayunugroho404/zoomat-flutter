// To parse this JSON data, do
//
//     final responsePostReview = responsePostReviewFromJson(jsonString);

import 'dart:convert';

ResponsePostReview responsePostReviewFromJson(String str) => ResponsePostReview.fromJson(json.decode(str));

String responsePostReviewToJson(ResponsePostReview data) => json.encode(data.toJson());

class ResponsePostReview {
  ResponsePostReview({
    this.review,
    this.message,
  });

  Review? review;
  String? message;

  factory ResponsePostReview.fromJson(Map<String, dynamic> json) => ResponsePostReview(
    review: json["review"] == null ? null : Review.fromJson(json["review"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "review": review == null ? null : review?.toJson(),
    "message": message == null ? null : message,
  };
}

class Review {
  Review({
    this.rate,
    this.userId,
    this.name,
    this.restoId,
    this.updatedAt,
    this.createdAt,
    this.id,
  });

  String? rate;
  String? userId;
  String? name;
  String? restoId;
  DateTime? updatedAt;
  DateTime? createdAt;
  int? id;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
    rate: json["rate"] == null ? null : json["rate"],
    userId: json["user_id"] == null ? null : json["user_id"],
    name: json["name"] == null ? null : json["name"],
    restoId: json["resto_id"] == null ? null : json["resto_id"],
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toJson() => {
    "rate": rate == null ? null : rate,
    "user_id": userId == null ? null : userId,
    "name": name == null ? null : name,
    "resto_id": restoId == null ? null : restoId,
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "id": id == null ? null : id,
  };
}
