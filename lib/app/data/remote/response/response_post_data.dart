// To parse this JSON data, do
//
//     final responsePostData = responsePostDataFromJson(jsonString);

import 'dart:convert';

ResponsePostData responsePostDataFromJson(String str) => ResponsePostData.fromJson(json.decode(str));

String responsePostDataToJson(ResponsePostData data) => json.encode(data.toJson());

class ResponsePostData {
  ResponsePostData({
    this.success,
    this.message,
    this.file,
  });

  bool? success;
  String? message;
  String? file;

  factory ResponsePostData.fromJson(Map<String, dynamic> json) => ResponsePostData(
    success: json["success"] == null ? null : json["success"],
    message: json["message"] == null ? null : json["message"],
    file: json["file"] == null ? null : json["file"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "message": message == null ? null : message,
    "file": file == null ? null : file,
  };
}
