
import 'dart:convert';

ResponseLogin responseLoginFromJson(String str) => ResponseLogin.fromJson(json.decode(str));

String responseLoginToJson(ResponseLogin data) => json.encode(data.toJson());

class ResponseLogin {
  ResponseLogin({
    this.statusCode,
    this.accessToken,
    this.user,
    this.tokenType,
  });

  int? statusCode;
  String? accessToken;
  User? user;
  String? tokenType;

  factory ResponseLogin.fromJson(Map<String, dynamic> json) => ResponseLogin(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    accessToken: json["access_token"] == null ? null : json["access_token"],
    user: json["user"] == null ? null : User.fromJson(json["user"]),
    tokenType: json["token_type"] == null ? null : json["token_type"],
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "access_token": accessToken == null ? null : accessToken,
    "user": user == null ? null : user?.toJson(),
    "token_type": tokenType == null ? null : tokenType,
  };
}

class User {
  User({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.phoneNumber,
    this.isUser,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? email;
  dynamic? emailVerifiedAt;
  String? phoneNumber;
  int? isUser;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    emailVerifiedAt: json["email_verified_at"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "email_verified_at": emailVerifiedAt,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "is_user": isUser == null ? null : isUser,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
