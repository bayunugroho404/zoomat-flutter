// To parse this JSON data, do
//
//     final responseGetRiviewByUser = responseGetRiviewByUserFromJson(jsonString);

import 'dart:convert';

ResponseGetRiviewByUser responseGetRiviewByUserFromJson(String str) => ResponseGetRiviewByUser.fromJson(json.decode(str));

String responseGetRiviewByUserToJson(ResponseGetRiviewByUser data) => json.encode(data.toJson());

class ResponseGetRiviewByUser {
  ResponseGetRiviewByUser({
    this.statusCode,
    this.data,
  });

  int? statusCode;
  List<Datum>? data;

  factory ResponseGetRiviewByUser.fromJson(Map<String, dynamic> json) => ResponseGetRiviewByUser(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.rate,
    this.userId,
    this.name,
    this.restoId,
    this.createdAt,
    this.updatedAt,
    this.users,
    this.restos,
  });

  int? id;
  String? rate;
  int? userId;
  String? name;
  int? restoId;
  DateTime? createdAt;
  DateTime? updatedAt;
  Users? users;
  Restos? restos;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    rate: json["rate"] == null ? null : json["rate"],
    userId: json["user_id"] == null ? null : json["user_id"],
    name: json["name"] == null ? null : json["name"],
    restoId: json["resto_id"] == null ? null : json["resto_id"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    users: json["users"] == null ? null : Users.fromJson(json["users"]),
    restos: json["restos"] == null ? null : Restos.fromJson(json["restos"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "rate": rate == null ? null : rate,
    "user_id": userId == null ? null : userId,
    "name": name == null ? null : name,
    "resto_id": restoId == null ? null : restoId,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "users": users == null ? null : users?.toJson(),
    "restos": restos == null ? null : restos?.toJson(),
  };
}

class Restos {
  Restos({
    this.id,
    this.name,
    this.address,
    this.lattitude,
    this.image,
    this.longtitude,
    this.timings,
    this.cost,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? address;
  String? lattitude;
  String? image;
  String? longtitude;
  String? timings;
  String? cost;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Restos.fromJson(Map<String, dynamic> json) => Restos(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    address: json["address"] == null ? null : json["address"],
    lattitude: json["lattitude"] == null ? null : json["lattitude"],
    image: json["image"] == null ? null : json["image"],
    longtitude: json["longtitude"] == null ? null : json["longtitude"],
    timings: json["timings"] == null ? null : json["timings"],
    cost: json["cost"] == null ? null : json["cost"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "address": address == null ? null : address,
    "lattitude": lattitude == null ? null : lattitude,
    "image": image == null ? null : image,
    "longtitude": longtitude == null ? null : longtitude,
    "timings": timings == null ? null : timings,
    "cost": cost == null ? null : cost,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}

class Users {
  Users({
    this.id,
    this.name,
    this.email,
    this.emailVerifiedAt,
    this.phoneNumber,
    this.isUser,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? email;
  dynamic? emailVerifiedAt;
  String? phoneNumber;
  int? isUser;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Users.fromJson(Map<String, dynamic> json) => Users(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    emailVerifiedAt: json["email_verified_at"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    isUser: json["is_user"] == null ? null : json["is_user"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "email_verified_at": emailVerifiedAt,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "is_user": isUser == null ? null : isUser,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
  };
}
