import 'package:get/get.dart';

import 'package:zoomat/app/modules/add_info/bindings/add_info_binding.dart';
import 'package:zoomat/app/modules/add_info/views/add_info_view.dart';
import 'package:zoomat/app/modules/add_menu/bindings/add_menu_binding.dart';
import 'package:zoomat/app/modules/add_menu/views/add_menu_view.dart';
import 'package:zoomat/app/modules/add_photo/bindings/add_photo_binding.dart';
import 'package:zoomat/app/modules/add_photo/views/add_photo_view.dart';
import 'package:zoomat/app/modules/add_resto/bindings/add_resto_binding.dart';
import 'package:zoomat/app/modules/add_resto/views/add_resto_view.dart';
import 'package:zoomat/app/modules/detail_photo/bindings/detail_photo_binding.dart';
import 'package:zoomat/app/modules/detail_photo/views/detail_photo_view.dart';
import 'package:zoomat/app/modules/home/bindings/home_binding.dart';
import 'package:zoomat/app/modules/home/views/home_view.dart';
import 'package:zoomat/app/modules/home_detail/bindings/home_detail_binding.dart';
import 'package:zoomat/app/modules/home_detail/views/home_detail_view.dart';
import 'package:zoomat/app/modules/home_user/bindings/home_user_binding.dart';
import 'package:zoomat/app/modules/home_user/views/home_user_view.dart';
import 'package:zoomat/app/modules/login/bindings/login_binding.dart';
import 'package:zoomat/app/modules/login/views/login_view.dart';
import 'package:zoomat/app/modules/register/bindings/register_binding.dart';
import 'package:zoomat/app/modules/register/views/register_view.dart';
import 'package:zoomat/app/modules/review_user/bindings/review_user_binding.dart';
import 'package:zoomat/app/modules/review_user/views/review_user_view.dart';
import 'package:zoomat/app/modules/setting/bindings/setting_binding.dart';
import 'package:zoomat/app/modules/setting/views/setting_view.dart';
import 'package:zoomat/app/modules/zoom_photo/bindings/zoom_photo_binding.dart';
import 'package:zoomat/app/modules/zoom_photo/views/zoom_photo_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  //page default
  static const INITIAL = Routes.HOME_USER;

  //route page
  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.HOME_DETAIL,
      page: () => HomeDetailView(),
      binding: HomeDetailBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_PHOTO,
      page: () => DetailPhotoView(),
      binding: DetailPhotoBinding(),
    ),
    GetPage(
      name: _Paths.ZOOM_PHOTO,
      page: () => ZoomPhotoView(),
      binding: ZoomPhotoBinding(),
    ),
    GetPage(
      name: _Paths.HOME_USER,
      page: () => HomeUserView(),
      binding: HomeUserBinding(),
    ),
    GetPage(
      name: _Paths.SETTING,
      page: () => SettingView(),
      binding: SettingBinding(),
    ),
    GetPage(
      name: _Paths.REVIEW_USER,
      page: () => ReviewUserView(),
      binding: ReviewUserBinding(),
    ),
    GetPage(
      name: _Paths.ADD_PHOTO,
      page: () => AddPhotoView(),
      binding: AddPhotoBinding(),
    ),
    GetPage(
      name: _Paths.ADD_MENU,
      page: () => AddMenuView(),
      binding: AddMenuBinding(),
    ),
    GetPage(
      name: _Paths.ADD_INFO,
      page: () => AddInfoView(),
      binding: AddInfoBinding(),
    ),
    GetPage(
      name: _Paths.ADD_RESTO,
      page: () => AddRestoView(),
      binding: AddRestoBinding(),
    ),
  ];
}
