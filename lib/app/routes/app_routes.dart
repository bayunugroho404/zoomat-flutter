part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart
abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const HOME_DETAIL = _Paths.HOME_DETAIL;
  static const DETAIL_PHOTO = _Paths.DETAIL_PHOTO;
  static const ZOOM_PHOTO = _Paths.ZOOM_PHOTO;
  static const HOME_USER = _Paths.HOME_USER;
  static const SETTING = _Paths.SETTING;
  static const REVIEW_USER = _Paths.REVIEW_USER;
  static const ADD_PHOTO = _Paths.ADD_PHOTO;
  static const ADD_MENU = _Paths.ADD_MENU;
  static const ADD_INFO = _Paths.ADD_INFO;
  static const ADD_RESTO = _Paths.ADD_RESTO;
}

abstract class _Paths {
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const HOME_DETAIL = '/home-detail';
  static const DETAIL_PHOTO = '/detail-photo';
  static const ZOOM_PHOTO = '/zoom-photo';
  static const HOME_USER = '/home-user';
  static const SETTING = '/setting';
  static const REVIEW_USER = '/review-user';
  static const ADD_PHOTO = '/add-photo';
  static const ADD_MENU = '/add-menu';
  static const ADD_INFO = '/add-info';
  static const ADD_RESTO = '/add-resto';
}
