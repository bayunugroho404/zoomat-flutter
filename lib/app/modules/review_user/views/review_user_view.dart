import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:get/get.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/review_user_controller.dart';

class ReviewUserView extends GetView<ReviewUserController> {
  final controller = Get.put(ReviewUserController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.getDataReview();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('Review'),
        centerTitle: true,
      ),
      body: Obx(() => controller.listReview.length > 0
          ? ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: controller.listReview.length,
              itemBuilder: (
                context,
                index,
              ) {
                return ListTile(
                  title: Text("${controller.listReview[index].restos?.name}"),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('"${controller.listReview[index].name}"'),
                      RatingBar.builder(
                        initialRating: double.parse(
                            controller.listReview[index].rate!),
                        minRating: 1,
                        itemSize: 15,
                        direction: Axis.horizontal,
                        allowHalfRating: false,
                        itemCount: 5,
                        itemPadding:
                        EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: primary,
                        ),
                        onRatingUpdate: (rating) {
                        },
                      )
                    ],
                  ),
                  isThreeLine: true,
                  trailing:  Text(controller.formatDate(controller.listReview[index].createdAt!)),
                );
              },
            )
          : Container()),
    );
  }
}
