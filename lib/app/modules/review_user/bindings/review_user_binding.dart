import 'package:get/get.dart';

import '../controllers/review_user_controller.dart';

class ReviewUserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ReviewUserController>(
      () => ReviewUserController(),
    );
  }
}
