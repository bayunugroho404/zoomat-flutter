import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/data/remote/response/response_get_review_by_user.dart';
import 'package:zoomat/utils/constant.dart';

class ReviewUserController extends GetxController {
  final listReview = <Datum>[].obs;
  final storage = new FlutterSecureStorage();
  final format = new DateFormat('dd-MM-yyyy hh:mm:ss');

  @override
  void onInit() {
    super.onInit();
    getDataReview();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
  }

  void getDataReview() async {
    listReview.clear();
    String token = (await storage.read(key: TOKEN))!;
    String id = (await storage.read(key: USER_ID))!;
    print('id' + id);
    update();
    await APIService().getReviewByUser(token, id).then((data) {
      listReview.value = data.data!;
      update();
    }).catchError((onError) {
      print(onError);
    });
  }

  String formatDate(DateTime date){
    String dateFormat = format.format(date);
    return dateFormat.toString();
  }
}
