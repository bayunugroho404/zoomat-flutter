import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/response/response_get_all_resto.dart';
import 'package:zoomat/app/models/model.dart';
import 'package:zoomat/app/modules/add_info/views/add_info_view.dart';
import 'package:zoomat/app/modules/add_menu/views/add_menu_view.dart';
import 'package:zoomat/app/modules/detail_photo/views/detail_photo_view.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/app/modules/zoom_photo/views/zoom_photo_view.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/home_detail_controller.dart';

class HomeDetailView extends GetView<HomeDetailController> {
  String? tag;
  String? id;
  String? no;
  RestaurantModel? data;
  String? path;
  String? name;

  HomeDetailView({this.tag, this.path, this.name, this.data, this.id,this.no});

  final controller = Get.put(HomeDetailController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.getMenuByResto(id!);
    controller.getStatus();
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Visibility(
              visible: controller.is_user.isEmpty?false:true,
              child: InkWell(
                onTap: () {
                  AwesomeDialog(
                      context: context,
                      animType: AnimType.SCALE,
                      dialogType: DialogType.QUESTION,
                      body: Center(
                        child: Column(
                          children: [
                            Text(
                              'Review $name',
                              style: TextStyle(fontStyle: FontStyle.italic),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            RatingBar.builder(
                              initialRating: 1,
                              minRating: 1,
                              itemSize: 30,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: primary,
                              ),
                              onRatingUpdate: (rating) {
                                controller.rate.value = rating.toInt().toString();
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextField(
                                keyboardType: TextInputType.multiline,
                                maxLines: 4,
                                controller: controller.reviewTextController,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.fromLTRB(
                                        20.0, 15.0, 20.0, 15.0),
                                    hintText: "...",
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0))),
                              ),
                            )
                          ],
                        ),
                      ),
                      title: 'This is Ignored',
                      desc: 'This is also Ignored',
                      btnOkOnPress: () {
                        controller.postReview(controller.rate.value,
                            controller.reviewTextController.text, id.toString());
                      },
                      btnOkText: "Submit",
                      btnCancelOnPress: () {})
                    ..show();
                },
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.zero,
                      bottomLeft: Radius.circular(30.0),
                      bottomRight: Radius.zero,
                    ),
                  ),
                  color: primary,
                  child: Container(
                    height: 60,
                    width: SizeConfig.screenWidth / 3,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Review',
                          style: TextStyle(
                              fontSize: SizeConfig.blockVertical * 2,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                controller.launchURL(
                    "https://www.google.com/maps/search/?api=1&query=${data?.lat},${data?.lng}");
              },
              child: Card(
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.zero,
                    topRight: Radius.circular(30.0),
                    bottomLeft: Radius.zero,
                    bottomRight: Radius.circular(30.0),
                  ),
                ),
                color: primary,
                child: Container(
                  height: 60,
                  width: SizeConfig.screenWidth / 3,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Direction',
                        style: TextStyle(
                            fontSize: SizeConfig.blockVertical * 2,
                            color: Colors.white),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Icon(
                        Icons.directions,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      body: Container(
        width: SizeConfig.screenWidth,
        child: ListView(
          children: [
            Stack(
              children: [
                Container(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHight / 2.5,
                  child: Hero(
                      tag: tag!,
                      child: Image.network(
                        path!,
                        fit: BoxFit.fill,
                      )),
                ),
                Positioned(
                  right: -10,
                  top: SizeConfig.screenHight / 20.5,
                  child: InkWell(
                    onTap: () {
                      AwesomeDialog(
                        context: context,
                        dialogType: DialogType.QUESTION,
                        animType: AnimType.BOTTOMSLIDE,
                        title: 'Phone Number',
                        desc: '$no',
                        btnOkOnPress: () {},
                      )..show();
                    },
                    child: Card(
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.zero,
                          bottomLeft: Radius.circular(30.0),
                          bottomRight: Radius.zero,
                        ),
                      ),
                      color: Colors.white,
                      child: Container(
                        height: 60,
                        width: SizeConfig.screenWidth / 4.5,
                        child: Center(child: Icon(Icons.call)),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: -10,
                  top: SizeConfig.screenHight / 6.5,
                  child: InkWell(
                    onTap: () {
                      if (controller.is_user.value == "0") {
                        AwesomeDialog(
                            context: context,
                            animType: AnimType.SCALE,
                            dialogType: DialogType.INFO,
                            body: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text('vehicle : ${controller.vehicle}'),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    TextField(
                                      controller: controller.vehicleController,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets.fromLTRB(
                                              20.0, 15.0, 20.0, 15.0),
                                          hintText: "Update Detail Vehicle",
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(32.0))),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            title: 'This is Ignored',
                            desc: 'This is also Ignored',
                            btnOkOnPress: () {
                              controller.updateVehicle(
                                  id!, controller.vehicleController.text);
                            },
                            btnOkText: "Update",
                            btnCancelOnPress: () {})
                          ..show();
                      } else {
                        AwesomeDialog(
                          context: context,
                          dialogType: DialogType.QUESTION,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'Vehicle Detail',
                          desc: '${controller.vehicle}',
                          btnOkOnPress: () {},
                        )..show();
                      }
                    },
                    child: Card(
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.zero,
                          bottomLeft: Radius.circular(30.0),
                          bottomRight: Radius.zero,
                        ),
                      ),
                      color: Colors.white,
                      child: Container(
                        height: 60,
                        width: SizeConfig.screenWidth / 4.5,
                        child: Center(child: Icon(Icons.directions_bike)),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  right: -10,
                  top: SizeConfig.screenHight / 3.5,
                  child: InkWell(
                    onTap: () {
                      Get.to(DetailPhotoView(
                          name: data!.name!, id: data!.id.toString()));
                    },
                    child: Card(
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          topRight: Radius.zero,
                          bottomLeft: Radius.circular(30.0),
                          bottomRight: Radius.zero,
                        ),
                      ),
                      color: Colors.white,
                      child: Container(
                        height: 60,
                        width: SizeConfig.screenWidth / 3.5,
                        child: Center(
                            child: Text(
                          'See All Photo',
                          style:
                              TextStyle(fontSize: SizeConfig.blockVertical * 2),
                        )),
                      ),
                    ),
                  ),
                ),
                Positioned(
                    top: 20,
                    left: 0,
                    child: IconButton(
                      icon: Icon(Icons.arrow_back_outlined),
                      color: Colors.white,
                      onPressed: () {
                        Get.back();
                      },
                    ))
              ],
            ),
            Container(
              height: SizeConfig.screenHight / 10,
              width: SizeConfig.screenWidth,
              padding: EdgeInsets.only(left: 20),
              color: Colors.grey[100],
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        data!.name! + ", ",
                        style:
                            TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                      Text(data!.address!, style: TextStyle(fontSize: 20)),
                      SizedBox(width: 10,),
                      Obx(()=>Visibility(
                        visible: controller.is_user.value == "0" ? true : false,
                        child: InkWell(
                          onTap: (){
                            AwesomeDialog(
                              context: context,
                              dialogType: DialogType.QUESTION,
                              animType: AnimType.BOTTOMSLIDE,
                              title: 'apakah anda yakin ?',
                              desc: 'Apakah anda yakin ingin menghapus resto ini?',
                              btnCancelOnPress: (){},
                              btnOkOnPress: () {
                                controller.remove_resto(id!);
                              },
                            )..show();
                          },
                          child: Card(child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Text("hapus resto", style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.red)),
                                Icon(Icons.delete,color: Colors.red)
                              ],
                            ),
                          )),
                        ),
                      )),
                    ],
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Card(
                    elevation: 8,
                    color: secondary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: SizedBox(
                      width: SizeConfig.screenWidth / 2.5,
                      height: 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.schedule,
                            color: Colors.white,
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          Obx(() => Text('${controller.timins}',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: SizeConfig.blockVertical * 2,
                                  fontWeight: FontWeight.bold))),
                        ],
                      ),
                    )),
                InkWell(
                  onTap: (){
                    if(controller.is_user.value == "0"){
                      AwesomeDialog(
                          context: context,
                          animType: AnimType.SCALE,
                          dialogType: DialogType.INFO,
                          body: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text('cost : ${controller.cost.value}'),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  TextField(
                                    controller: controller.costController,
                                    keyboardType:TextInputType.number,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.fromLTRB(
                                            20.0, 15.0, 20.0, 15.0),
                                        hintText: "Update Cost Detail",
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            BorderRadius.circular(32.0))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          title: 'This is Ignored',
                          desc: 'This is also Ignored',
                          btnOkOnPress: () {
                            controller.updateCost(
                                id!, controller.costController.text);
                          },
                          btnOkText: "Update",
                          btnCancelOnPress: () {})
                        ..show();
                    }
                  },
                  child: Card(
                      elevation: 8,
                      color: secondary,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: SizedBox(
                        width: SizeConfig.screenWidth / 2.5,
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.monetization_on,
                              color: Colors.white,
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Obx(() => Text('Rp. ${controller.cost.value}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: SizeConfig.blockVertical * 2,
                                    fontWeight: FontWeight.bold))),
                          ],
                        ),
                      )),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 20),
              child: Row(
                children: [
                  Text(
                    'Menu',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Obx(() => Visibility(
                        visible: controller.is_user.value == "0" ? true : false,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddMenuView(
                                        name: name,
                                        id: id,
                                        refresh: controller.getMenuByResto(
                                            id!)))).then((value) {
                              controller.getMenuByResto(id!);
                            });
                          },
                          child: Card(
                            elevation: 4,
                            color: secondary,
                            child: Container(
                              margin: EdgeInsets.all(4),
                              child: Row(
                                children: [
                                  Icon(Icons.add),
                                  Text(
                                    'Menu',
                                    style: TextStyle(fontSize: 20),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 10,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Obx(() => controller.listMenu.length > 0
                ? GridView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: controller.listMenu.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 2,
                    ),
                    itemBuilder: (
                      context,
                      index,
                    ) {
                      return Hero(
                        tag: "menus" + index.toString(),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ZoomPhotoView(
                                        tag: "menus" + index.toString(),
                                        title:"menu",
                                        path: controller.listMenu[index].name!,
                                        id: controller.listMenu[index].id!,
                                        is_user: controller.is_user.value == "0"
                                            ? true
                                            : false,
                                        refresh: controller.getMenuByResto(
                                            id!)))).then((value) {
                              controller.getMenuByResto(id!);
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.white)),
                            child: Image.network(
                              controller.listMenu[index].name!,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    },
                  )
                : Container()),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 30),
              child: Text(
                'Review',
                style: TextStyle(fontSize: 20),
              ),
            ),
            Obx(() => controller.listReview.length > 0
                ? ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: controller.listReview.length,
                    itemBuilder: (
                      context,
                      index,
                    ) {
                      return Container(
                        margin: EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(controller.listReview[index].users!.name!),
                                SizedBox(
                                  width: 4,
                                ),
                                RatingBar.builder(
                                  initialRating: double.parse(
                                      controller.listReview[index].rate!),
                                  minRating: 1,
                                  itemSize: 15,
                                  direction: Axis.horizontal,
                                  allowHalfRating: false,
                                  itemCount: 5,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 4.0),
                                  itemBuilder: (context, _) => Icon(
                                    Icons.star,
                                    color: primary,
                                  ),
                                  onRatingUpdate: (rating) {},
                                )
                              ],
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text('"${controller.listReview[index].name!}"'),
                            Divider()
                          ],
                        ),
                      );
                    },
                  )
                : Container()),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 20),
              child: Row(
                children: [
                  Text(
                    'Other Info',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Obx(()=>Visibility(
                    visible: controller.is_user.value == "0" ? true : false,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddInfoView(
                                    name: name,
                                    id: id,
                                    refresh: controller
                                        .getMenuByResto(id!)))).then((value) {
                          controller.getMenuByResto(id!);
                        });
                      },
                      child: Card(
                        elevation: 4,
                        color: secondary,
                        child: Container(
                          margin: EdgeInsets.all(4),
                          child: Row(
                            children: [
                              Icon(Icons.add),
                              Text(
                                'Info',
                                style: TextStyle(fontSize: 20),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )),
                  SizedBox(
                    width: 10,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Obx(() => controller.listInfo.length > 0
                ? GridView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: controller.listInfo.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 4,
                    ),
                    itemBuilder: (
                      context,
                      index,
                    ) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.check_circle_outline,
                              color: Colors.green,
                            ),
                            Text(controller.listInfo[index].name!),
                          ],
                        )),
                      );
                    },
                  )
                : Container()),
            SizedBox(
              height: 100,
            )
          ],
        ),
      ),
    );
  }
}
