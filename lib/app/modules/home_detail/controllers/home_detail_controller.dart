import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/data/remote/response/response_get_info_by_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_menu_by_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_review_by_resto.dart';
import 'package:zoomat/app/modules/home_user/views/home_user_view.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

class HomeDetailController extends GetxController {
  final storage = new FlutterSecureStorage();
  final count = 0.obs;
  final timins = ''.obs;
  final vehicle = ''.obs;
  final cost = ''.obs;
  final rate = ''.obs;
  TextEditingController reviewTextController = TextEditingController();
  TextEditingController vehicleController = TextEditingController();
  TextEditingController costController = TextEditingController();
  final listMenu = <Menu>[].obs;
  final is_user = ''.obs;
  final listReview = <Review>[].obs;
  final listInfo = <Info>[].obs;

  @override
  void onInit() {
    super.onInit();
    getStatus();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> getMenuByResto(String id) async {
    listMenu.clear();
    listReview.clear();
    listInfo.clear();
    vehicle.value = '...';
    // String token = (await storage.read(key: TOKEN))!;
    update();

    await APIService().getMenuByResto( id).then((data) {
      //ambil data menu by resto
      listMenu.value = data.menus!;

      //ambil data timing ,vehicle ,cost
      timins.value = data.resto!.timings!;
      vehicle.value = data.resto!.vehicle!;
      cost.value = data.resto!.cost!;
      update();
    }).catchError((onError) {
      print(onError);
    });

    //ambil data review by resto
    await APIService().getReviewByResto( id).then((data) {
      listReview.value = data.reviews!;
      update();
    }).catchError((onError) {
      print(onError);
    });


    //ambil data info by resto
    await APIService().getInfoByResto( id).then((data) {
      listInfo.value = data.infos!;
      update();
    }).catchError((onError) {
      print(onError);
    });

    return true;
  }

  void postReview(String rate,String name,String resto_id)async{
    String token = (await storage.read(key: TOKEN))!;
    String user_id = (await storage.read(key: USER_ID))!;
    update();
    APIService().postReview(token, rate, user_id, name, resto_id).then((data){
      if(data.message == "Created successfully"){
        showToast(data.message!);
        getMenuByResto(resto_id);
      }
    }).catchError((onError){
      showToast('gagal review');
    });
  }

  //example "https://www.google.com/maps/search/?api=1&query=lat resto,$lng resto");
  void launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';

  void getStatus() async{
     is_user.value = (await storage.read(key: IS_USER))!;
     update();
  }

  void updateVehicle(String id,String text)async{
    String token = (await storage.read(key: TOKEN))!;
    APIService().updateVehicle(token, id, text).then((val){
      if(val.status == "successfully"){
        showToast("Berhasil");
      }
      vehicleController.text = '';
      getMenuByResto(id);
    }).catchError((_){});
  }

  void updateCost(String id,String text)async{
    String token = (await storage.read(key: TOKEN))!;
    APIService().updateCost(token, id, text).then((val){
      if(val.message == "successfully"){
        showToast("Berhasil");
      }
      costController.text = '';
      getMenuByResto(id);
    }).catchError((_){});
  }

  void remove_resto(String id)async{
    String token = (await storage.read(key: TOKEN))!;
    APIService().remove_resto(token, id).then((val){
      if(val.message == "successfully"){
        showToast("Berhasil");
        Get.offAll(HomeUserView());
      }
    }).catchError((_){});
  }
}
