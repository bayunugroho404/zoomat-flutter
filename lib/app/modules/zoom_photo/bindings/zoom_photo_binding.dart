import 'package:get/get.dart';

import '../controllers/zoom_photo_controller.dart';

class ZoomPhotoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ZoomPhotoController>(
      () => ZoomPhotoController(),
    );
  }
}
