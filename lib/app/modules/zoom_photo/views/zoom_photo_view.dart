import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';

import '../controllers/zoom_photo_controller.dart';

class ZoomPhotoView extends GetView<ZoomPhotoController> {
  String? tag;
  String? path;
  String? title;
  int? id;
  bool? is_user;
  Future<bool>? refresh;


  final controller = Get.put(ZoomPhotoController());

  ZoomPhotoView({this.tag, this.path,this.id,this.is_user,this.refresh,this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 30),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                      ),
                      onPressed: () => Navigator.of(context).pop(),
                      child: Icon(Icons.close,color: Colors.grey,),
                    ),
                  ),
                  Spacer(),
                  Visibility(
                      visible: is_user == true?true:false,
                      child: IconButton(icon: Icon(Icons.delete), onPressed: (){
                        AwesomeDialog(
                          context: context,
                          dialogType: DialogType.QUESTION,
                          animType: AnimType.BOTTOMSLIDE,
                          title: 'apakah anda yakin ?',
                          desc: 'Apakah anda yakin ingin menghapus?',
                          btnCancelOnPress: (){},
                          btnOkOnPress: () {
                            if(title == "photo"){
                              controller.remove_photo(id.toString());
                            }else if(title == "menu" ){
                              controller.remove_menu(id.toString());
                            }
                          },
                        )..show();
                      }))
                ],
              ),
              Container ( width: MediaQuery
                  .of(context)
                  .size
                  .width,
                  child:
                  Hero(
                      tag: tag!,
                      child: Image.network(path!, fit: BoxFit.fitWidth ))
              ),
            ],
          ),
        ),
      ),
    );
  }
}
