import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

class ZoomPhotoController extends GetxController {
  //TODO: Implement ZoomPhotoController
  final storage = new FlutterSecureStorage();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void remove_menu(String id)async{
    String token = (await storage.read(key: TOKEN))!;
    APIService().remove_menu(token, id).then((val){
    if(val.message == "successfully"){
      showToast("Berhasil");
      Get.back();
    }
    }).catchError((_){});
  }

  void remove_photo(String id)async{
    String token = (await storage.read(key: TOKEN))!;
    APIService().remove_photo(token, id).then((val){
      if(val.message == "successfully"){
        showToast("Berhasil");
        Get.back();
      }
    }).catchError((_){});
  }

}
