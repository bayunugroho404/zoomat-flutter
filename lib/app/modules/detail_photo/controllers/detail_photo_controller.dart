import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/data/remote/response/response_get_photo_by_resto.dart';
import 'package:zoomat/utils/constant.dart';

class DetailPhotoController extends GetxController {
  final listPhoto = <Photo>[].obs;
  final is_user = ''.obs;
  final storage = new FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
    getStatus();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> getPhotoByResto(String id) async {
    listPhoto.clear();
    print('refresah');
    // String token = (await storage.read(key: TOKEN))!;
    update();
    await APIService().getPhotoByResto( id).then((data) {
      listPhoto.value = data.photos!;
      update();
    }).catchError((onError) {
      print(onError);
    });
    update();
    return true;
  }

  void getStatus() async{
    is_user.value = (await storage.read(key: IS_USER))!;
    update();
  }
}
