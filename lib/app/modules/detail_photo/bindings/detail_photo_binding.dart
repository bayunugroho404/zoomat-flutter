import 'package:get/get.dart';

import '../controllers/detail_photo_controller.dart';

class DetailPhotoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailPhotoController>(
      () => DetailPhotoController(),
    );
  }
}
