import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/app/modules/add_photo/views/add_photo_view.dart';
import 'package:zoomat/app/modules/zoom_photo/views/zoom_photo_view.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/detail_photo_controller.dart';

class DetailPhotoView extends GetView<DetailPhotoController> {
  String? id;
  String? name;

  DetailPhotoView({this.id, this.name});

  final controller = Get.put(DetailPhotoController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.getPhotoByResto(id!);
    controller.getStatus();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text(name!),
        actions: [Padding(
          padding: const EdgeInsets.all(8.0),
          child: Obx(()=>Visibility(
              visible: controller.is_user.value == "0"?true:false,
              child: IconButton(icon:Icon(Icons.add), onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    AddPhotoView( name: name, id: id,refresh: controller.getPhotoByResto(id!)))).then((value) {
                  controller.getPhotoByResto(id!);
                });
              },))),
        )],
        centerTitle: true,
      ),
      body: Obx(() => controller.listPhoto.length > 0
          ? GridView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: controller.listPhoto.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2,
              ),
              itemBuilder: (
                context,
                index,
              ) {
                return Hero(
                  tag: "photos" + index.toString(),
                  child: GestureDetector(
                    onTap: () {

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ZoomPhotoView(
                                  tag: "menus" + index.toString(),
                                  title:"photo",
                                  path: controller.listPhoto[index].name!,
                                  id: controller.listPhoto[index].id!,
                                  is_user: controller.is_user.value == "0"
                                      ? true
                                      : false,
                                  refresh: controller.getPhotoByResto(
                                      id!)))).then((value) {
                        controller.getPhotoByResto(id!);
                        controller.getStatus();
                      });

                      // Get.to(ZoomPhotoView(
                      //   tag: "photos" + index.toString(),
                      //   path: controller.listPhoto[index].name!,
                      // ));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white)),
                      child: Image.network(
                        controller.listPhoto[index].name!,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                );
              },
            )
          : Container()),
    );
  }
}
