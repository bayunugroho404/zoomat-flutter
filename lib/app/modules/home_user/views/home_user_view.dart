import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/utils/constant.dart';

import '../controllers/home_user_controller.dart';

class HomeUserView extends GetView<HomeUserController> {
  GlobalKey _bottomNavigationKey = GlobalKey();

  final controller =    Get.put(HomeUserController());

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        key: _bottomNavigationKey,
        child: Container(
          margin: EdgeInsets.only(left: 50.0, right: 50.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                onPressed: () {
                  controller.updateTabSelection(0, "Home");
                },
                iconSize: 23.0,
                icon: Obx(
                      () => Icon(
                  CupertinoIcons.home,
                  color: controller.selectedIndex.value == 0
                      ? primary
                      : Colors.grey.shade400,
                )),
              ),
              SizedBox(
                width: 10.0,
              ),
              IconButton(
                  onPressed: () {
                    controller.updateTabSelection(1, "Settings");
                  },
                  iconSize: 23.0,
                  icon: Obx(
                    () => Icon(
                      CupertinoIcons.gear_alt_fill,
                      color: controller.selectedIndex.value == 1
                          ? primary
                          : Colors.grey.shade400,
                    ),
                  )),
            ],
          ),
        ),
        shape: CircularNotchedRectangle(),
        color: Colors.white,
      ),
      body:Obx(
    () =>controller.tabs[controller.selectedIndex.value]),
    );
  }
}
