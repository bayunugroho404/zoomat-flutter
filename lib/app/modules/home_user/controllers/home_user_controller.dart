import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/modules/home/views/home_view.dart';
import 'package:zoomat/app/modules/setting/views/setting_view.dart';

class HomeUserController extends GetxController {
  final selectedIndex = 0.obs;

  final tabs = [
    HomeView(),
    SettingView()
  ];

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void updateTabSelection(int index, String buttonText) {
      selectedIndex.value = index;
  }
}
