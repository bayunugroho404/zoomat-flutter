import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/data/remote/config/apiServiceImage.dart';
import 'package:zoomat/app/data/remote/response/response_get_category.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

class AddRestoController extends GetxController {
  PickedFile? imageFile;
  final storage = new FlutterSecureStorage();
  dynamic pickImageError;
  final valProvince ="nul".obs;
  final listCategory = <ResponseGetCategoryResto>[].obs;
  final _picker = ImagePicker();

  TextEditingController nameController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController latController = new TextEditingController();
  TextEditingController lngController = new TextEditingController();
  TextEditingController timingsController = new TextEditingController();
  TextEditingController costController = new TextEditingController();
  TextEditingController vehicleController = new TextEditingController();


  @override
  void onInit() {
    getCategory();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future getImageFromGallery() async {
    try {
      final pickedFile = await _picker.getImage(
        source: ImageSource.gallery,
      );
      imageFile = pickedFile;
      print(imageFile?.path);
    } catch (e) {
      pickImageError = e;
    }
    update();
  }
  void addResto(Future<bool> refresh,
      PickedFile image,
      String name,
      String lattitude,
      String longtitude,
      String timings,
      String address,
      String cost,
      String vehicle,
      String phone,
      BuildContext context)async{
    String token = (await storage.read(key: TOKEN))!;
    if(imageFile == null ){
      showToast("gambar tidak boleh kosong");
    }else if(name.isEmpty || lattitude.isEmpty|| longtitude.isEmpty|| timings.isEmpty|| address.isEmpty|| cost.isEmpty){
      showToast("data tidak boleh kosong");
    }else{
      ApiServiceImage().addResto(token, image, name, address, lattitude, longtitude, timings, cost,vehicle,phone,valProvince.value).then((data){
        if(data.message =="Data successfully uploaded"){
          Navigator.pop(context);
        }else{
          print(data.message);
        }
      }).catchError((onError){
        print("Gagal tambah resto");
      });
    }
  }

  void getCategory() async{
    // String token = (await storage.read(key: TOKEN))!;
    APIService().getCategory().then((val){
      listCategory.value = val.resto!;
      update();
    }).catchError((onError){});
  }

}
