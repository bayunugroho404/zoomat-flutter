import 'dart:io';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/utils/constant.dart';

import '../controllers/add_resto_controller.dart';

class AddRestoView extends GetView<AddRestoController> {
  Future<bool>? refresh;
  AddRestoView({this.refresh});

  final controller = Get.put(AddRestoController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AddRestoView'),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  controller: controller.nameController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "Resto Name",
                      labelText: 'Resto Name',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  controller: controller.addressController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "Resto Address",
                      labelText: 'Resto Address',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),

              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Obx(()=>DropdownButton(
                  hint: Text("Select Category"),
                  value: controller.valProvince.value == "nul" ? null : controller.valProvince.value,
                  items: controller.listCategory.map((item){
                    return DropdownMenuItem(
                      child: Text("${item.name!}"),
                      value: item.id.toString(),
                    );
                  }).toList(),
                  onChanged: (value) {
                    controller.valProvince.value = value.toString();
                  },
                )),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  controller: controller.phoneController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "Resto Phone",
                      labelText: 'Resto Phone',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  controller: controller.latController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "(example : -6.350795)",
                      labelText: 'Resto Lattitude',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  controller: controller.lngController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "(example : 106.835683)",
                      labelText: 'Resto Longitude',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  controller: controller.timingsController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "Resto Timings",
                      labelText: 'Resto Timings',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 1.8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child:  TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  controller: controller.vehicleController,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      hintText: "Resto Vehicle",
                      labelText: 'Resto Vehicle',
                      hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Rp. '),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    width: MediaQuery.of(context).size.width * 0.8,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child:  TextField(
                      keyboardType: TextInputType.number,
                      controller: controller.costController,
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                          hintText: "Resto Cost Estimation",
                          labelText: 'Resto Cost Estimation',
                          hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                    ),
                  ),
                ],
              ),
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: GetBuilder<AddRestoController>(
                  builder: (_){
                    return Column(
                      children: [
                        controller.imageFile != null ?Semantics(
                            child: Image.file(File(controller.imageFile!.path)),
                            label: 'image_picker_example_picked_image'):Text('Image not selected'),
                        ListTile(
                          title: Padding(
                            padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                            child: new RaisedButton(
                              child: Text(
                                  "Choose Image"),
                              color: primary,
                              textColor: Colors.white,
                              onPressed: () {
                                controller.getImageFromGallery();
                              },
                            ),
                          ),
                        ),
                        Visibility(
                          visible: controller.imageFile != null?true:false,
                          child: ListTile(
                            title: Padding(
                              padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                              child: new RaisedButton(
                                child: Text(
                                    "Upload"),
                                color: primary,
                                textColor: Colors.white,
                                onPressed: () {
                                  controller.addResto(refresh!, controller.imageFile!,
                                      controller.nameController.text,
                                      controller.latController.text,
                                      controller.lngController.text,
                                      controller.timingsController.text,
                                      controller.addressController.text,
                                      controller.costController.text,
                                      controller.vehicleController.text,
                                      controller.phoneController.text,
                                      context);
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
