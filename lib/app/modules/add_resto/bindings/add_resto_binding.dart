import 'package:get/get.dart';

import '../controllers/add_resto_controller.dart';

class AddRestoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddRestoController>(
      () => AddRestoController(),
    );
  }
}
