import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/app/modules/register/views/register_view.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        height: SizeConfig.screenHight,
        width: SizeConfig.screenWidth,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: SizeConfig.screenHight / 8,),
              ClipOval(
                child: Image.asset(
                  'assets/jkt.png',
                  width: 100,
                  height: 100,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 40,),
              TextField(
                controller: emailController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Email",
                    border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 25.0),
              TextField(
                obscureText: true,
                controller: passwordController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Password",
                    border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
              ),
              SizedBox(height: 25.0),
              Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(30.0),
                color: primary,
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () {
                    controller.login(context, emailController.text, passwordController.text);
                  },
                  child: Text("Login",
                      textAlign: TextAlign.center,),
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Belum punya akun ? '),
                  InkWell(
                      onTap: (){Get.to(RegisterView());},
                      child: Text('Daftar',style: TextStyle(color: Colors.blue),)),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
