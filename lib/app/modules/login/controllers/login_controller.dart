import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/data/remote/response/response_login.dart';
import 'package:zoomat/app/modules/home/views/home_view.dart';
import 'package:zoomat/app/modules/home_user/views/home_user_view.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

class LoginController extends GetxController {
  //TODO: Implement LoginController

  final count = 0.obs;
  final token = ''.obs;
  final storage = new FlutterSecureStorage();


  //ketika aplikasi pertama kali dijalankan
  @override
  void onInit() {
    super.onInit();
    checkUser();
  }


  void login(BuildContext context, String email, String password) {
    //validasi email dan password
    if (email.length < 1) {
      showToast("email tidak boleh kosong");
    } else if (password.length < 1) {
      showToast("password tidak boleh kosong");
    } else {
      //login proses
      APIService().postLogin(email, password).then((data) {
        if (data.statusCode == 200) {
          showToast("sukses login");
          saveToLocal(data);
        } else {
          showToast("gagal login");
        }
      }).catchError((error) {
        print(error);
        showToast("$error");
      });
    }
  }

  void saveToLocal(ResponseLogin data) async{
    await storage.write(key: USER_ID, value: data.user?.id.toString());
    await storage.write(key: IS_USER, value: data.user?.isUser.toString());
    await storage.write(key: TOKEN, value: data.accessToken);
    await storage.write(key: PHONE_NUMBER, value: data.user?.phoneNumber);
    await storage.write(key: EMAIL, value: data.user?.email);
    await storage.write(key: NAME, value: data.user?.name);
    Get.offAll(HomeUserView());
  }

  void checkUser()async {
    token.value = (await storage.read(key: TOKEN))!;
    update();

    //jika token ada,maka langsung ke tampilan home
    if(token.value != null || token.value != "TOKEN"){
      Get.offAll(HomeUserView());
    }
  }
}
