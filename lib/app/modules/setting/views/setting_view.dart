import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/app/modules/login/views/login_view.dart';
import 'package:zoomat/app/modules/review_user/views/review_user_view.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/setting_controller.dart';

class SettingView extends GetView<SettingController> {
  final controller = Get.put(SettingController());

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.getDataUser();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('Setting'),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: SizeConfig.screenHight / 10,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                child: Obx(()=>Visibility(
                  visible: controller.name.value.isEmpty ? false:true,
                  child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 10.0,
                                offset: Offset(4.0, 4.0),
                                spreadRadius: 1.0)
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                    margin: const EdgeInsets.only(
                                        left: 10.0, right: 20.0),
                                    child: Divider(
                                      color: Colors.black,
                                      height: 36,
                                    )),
                              ),
                              Text("Profile"),
                              Expanded(
                                child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 10.0),
                                    child: Divider(
                                      color: Colors.black,
                                      height: 36,
                                    )),
                              ),
                            ],
                          ),
                          // Expanded(child: Divider()),
                          Obx(() => ListTile(
                            leading: Icon(CupertinoIcons.person),
                            title: Text('FullName'),
                            subtitle: Text('${controller.name.value}'),
                            onTap: () {},

                          )),
                          Obx(() => ListTile(
                            leading: Icon(CupertinoIcons.mail),
                            title: Text('Email'),
                            subtitle: Text('${controller.email.value}'),
                            onTap: () {},
                          )),
                          Obx(() => ListTile(
                            leading: Icon(CupertinoIcons.tray),
                            title: Text('Phone Number'),
                            subtitle: Text('${controller.phoneNumber.value}'),
                            onTap: () {},
                          )),
                          ListTile(
                            leading: Icon(Icons.edit),
                            title: Text('Edit Profile'),
                            onTap: () {
                              AwesomeDialog(
                                  context: context,
                                  animType: AnimType.SCALE,
                                  dialogType: DialogType.QUESTION,
                                  body: Center(
                                    child: Column(
                                      children: [
                                        Text(
                                          'Update Profile',
                                          style: TextStyle(fontStyle: FontStyle.italic),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            keyboardType: TextInputType.multiline,
                                            controller: nameController,
                                            enabled: controller.is_user.value == "true"?false:true,
                                            decoration: InputDecoration(
                                                labelText: "name",
                                                contentPadding: EdgeInsets.fromLTRB(
                                                    20.0, 15.0, 20.0, 15.0),
                                                hintText: "...",
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(10.0))),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            keyboardType: TextInputType.multiline,
                                            controller: emailController,
                                            decoration: InputDecoration(
                                                labelText: "email",
                                                contentPadding: EdgeInsets.fromLTRB(
                                                    20.0, 15.0, 20.0, 15.0),
                                                hintText: "...",
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(10.0))),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextField(
                                            keyboardType: TextInputType.multiline,
                                            controller: addressController,
                                            decoration: InputDecoration(
                                                labelText: "Phone Number",
                                                contentPadding: EdgeInsets.fromLTRB(
                                                    20.0, 15.0, 20.0, 15.0),
                                                hintText: "...",
                                                border: OutlineInputBorder(
                                                    borderRadius:
                                                    BorderRadius.circular(10.0))),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  title: 'This is Ignored',
                                  desc: 'This is also Ignored',
                                  btnOkOnPress: () {
                                    controller.updateProfile(nameController.text,emailController.text,addressController.text);
                                  },
                                  btnOkText: "Update",
                                  btnCancelOnPress: () {

                                  })
                                ..show();
                            },
                          ),

                        ],
                      )),
                )),
              ),
              Obx(()=>Visibility(
                visible: controller.name.value.isEmpty ? false:true,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                  child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 10.0,
                                offset: Offset(4.0, 4.0),
                                spreadRadius: 1.0)
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                    margin: const EdgeInsets.only(
                                        left: 10.0, right: 20.0),
                                    child: Divider(
                                      color: Colors.black,
                                      height: 36,
                                    )),
                              ),
                              Text("Activity"),
                              Expanded(
                                child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 10.0),
                                    child: Divider(
                                      color: Colors.black,
                                      height: 36,
                                    )),
                              ),
                            ],
                          ),
                          // Expanded(child: Divider()),
                          ListTile(
                            leading: Icon(CupertinoIcons.tag),
                            title: Text('Review Activity'),
                            trailing: Icon(CupertinoIcons.chevron_right),
                            onTap: () {
                              Get.to(ReviewUserView());
                            },
                          ),
                        ],
                      )),
                ),
              )),
              Obx(()=>Visibility(
                visible: controller.name.value.isEmpty ? false:true,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                  child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 10.0,
                                offset: Offset(4.0, 4.0),
                                spreadRadius: 1.0)
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(CupertinoIcons.square_arrow_left),
                            title: Text('Logout'),
                            onTap: () {
                              controller.doLogout();
                            },
                          ),
                        ],
                      )),
                ),
              )),
              Obx(()=>Visibility(
                visible: controller.name.value.isEmpty ? true:false,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5.0, right: 30, left: 30),
                  child: Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 10.0,
                                offset: Offset(4.0, 4.0),
                                spreadRadius: 1.0)
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(CupertinoIcons.square_arrow_left),
                            title: Text('Login'),
                            onTap: () {
                              Get.offAll(LoginView());
                            },
                          ),
                        ],
                      )),
                ),
              )),
            ],
          ),
        ),
      ),
    );
  }
}
