import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/modules/home_user/views/home_user_view.dart';
import 'package:zoomat/app/modules/login/views/login_view.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

class SettingController extends GetxController {
  final storage = new FlutterSecureStorage();
  final name = ''.obs;
  final email = ''.obs;
  final phoneNumber = ''.obs;
  final is_user = 'false'.obs;

  @override
  void onInit() {
    super.onInit();
    getDataUser();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getDataUser() async{
    name.value = (await storage.read(key: NAME))!;
    email.value = (await storage.read(key: EMAIL))!;
    is_user.value = (await storage.read(key: IS_USER))!;
    phoneNumber.value = (await storage.read(key: PHONE_NUMBER))!;
    print("sahs " + is_user.value);
    update();
  }

  void doLogout()async{
    await storage.deleteAll();
    Get.offAll(HomeUserView());
  }
  void updateProfile(String name, String email, String no) async{
    String token = (await storage.read(key: TOKEN))!;

    APIService().updateProfile(token,name,no,email).then((val){
      if(val.message == "OK"){
        showToast("Profile update Successfull");
        saveToLocal(email,name,no);
        getDataUser();
      }
    });
  }

  void saveToLocal(String email, String name, String address) async{
    await storage.write(key: NAME, value:name);
    await storage.write(key: EMAIL, value:email);
    await storage.write(key: PHONE_NUMBER, value:address);
  }
}
