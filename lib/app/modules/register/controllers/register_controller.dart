import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';

class RegisterController extends GetxController {
  //TODO: Implement RegisterController

  final count = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  void register(String name,
      String phone_number,
      String email,
      String password,
      String password_confirmation) {
    if (name.isNotEmpty && phone_number.isNotEmpty && email.isNotEmpty &&
        password.isNotEmpty && password_confirmation.isNotEmpty) {
      if (password != password_confirmation) {
        showToast("password tidak sama");
      } else {
        APIService().postRegister(
            name, phone_number, email, password, password_confirmation).then((
            val) {
          if (val.message == "Registration Successfull") {
            Get.back();
            showToast("registrasi berhasil");
          } else {
            showToast("registrasi gagal");
          }
        }).catchError((onError){
          showToast("akun sudah terdaftar");
        });
      }
    } else {
      showToast("data tidak boleh kosong");
    }
  }

}
