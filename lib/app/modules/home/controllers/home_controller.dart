import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/data/remote/response/response_get_all_resto.dart';
import 'package:zoomat/app/data/remote/response/response_get_category.dart';
import 'package:zoomat/app/models/model.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

class HomeController extends GetxController {
  final storage = new FlutterSecureStorage();
  final count = 0.obs;
  final currentLat = 0.0.obs;
  final is_user = ''.obs;
  final currentLng = 0.0.obs;
  final current = 0.obs;
  final idByCurrent = 0.obs;

  final listAllRestos = <RestaurantModel>[].obs;
  final listCategory = <ResponseGetCategoryResto>[].obs;

  final filter = ''.obs;
  TextEditingController searchController = new TextEditingController();

  @override
  void onInit() {
    super.onInit();
    getStatus();
    searchController.addListener(() {
      filter.value = searchController.text;
      update();
    });
    _determinePosition();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() => count.value++;

  Future<bool> getAllRestaurant(String category_id) async {
    listAllRestos.clear();
    // String token = (await storage.read(key: TOKEN))!;
    APIService().getAllRestaurantByCategory(category_id).then((data) {
      for (int i = 0; i < data.resto!.length; i++) {
        listAllRestos.add(new RestaurantModel(
          name: data.resto![i].name,
          image: data.resto![i].image,
          id: data.resto![i].id,
          no: data.resto![i].desc,
          meter: setMeters(data.resto![i]),
          address: data.resto![i].address,
          lat: double.parse(data.resto![i].lattitude!),
          lng: double.parse(data.resto![i].longtitude!),
        ));
        //sort ascending restof

      }
      if (data.resto!.length < 1) {
        Future.delayed(Duration(seconds: 1), () {
          showToast("tidak ada resto");
        });
        listAllRestos.sort((a, b) => a.meter!.compareTo(b.meter!));
        listAllRestos.toSet().toList();
        update();
      }
    }).catchError((onError) {
      print(onError);
    });
    return true;
  }

  void getCurrentLocation() async {
    Position position = (await Geolocator.getLastKnownPosition())!;
    currentLat.value = position.latitude;
    currentLng.value = position.longitude;
    update();
    getCategory();
    // getAllRestaurant();
  }

  Future<Position> _determinePosition() async {
    /// Determine the current position of the device.
    ///
    /// When the location services are not enabled or permissions
    /// are denied the `Future` will return an error.
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    getCurrentLocation();
    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }


  //menghitung jarak
  setMeters(Resto resto) {
    double distanceInMeters = Geolocator.distanceBetween(
        double.parse(resto.lattitude!),
        double.parse(resto.longtitude!),
        currentLat.value,
        currentLng.value);
    //output example 25.6 km
    return distanceInMeters;
  }

  void getStatus() async {
    is_user.value = '1';
    is_user.value = (await storage.read(key: IS_USER))!;
    update();
  }

  void getCategory() async {
    listCategory.clear();
    // String token = (await storage.read(key: TOKEN))!;
    APIService().getCategory().then((val) {
      listCategory.value = val.resto!;
      update();
      if (listCategory.length > 0) {
        Future.delayed(Duration(seconds: 1), () {
          getAllRestaurant(listCategory[0].id.toString());
        });
      }
    }).catchError((_) {});
  }
}
