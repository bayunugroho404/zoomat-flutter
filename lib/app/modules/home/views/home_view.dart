import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/app/modules/add_resto/views/add_resto_view.dart';
import 'package:zoomat/app/modules/home_detail/views/home_detail_view.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/home_controller.dart';

import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final controller = Get.put(HomeController());

  @override
  void initState() {
    controller.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    controller.getStatus();
    return Scaffold(
      appBar: AppBar(
        title: Text('jktgo'),
        backgroundColor: primary,
        centerTitle: true,
        actions: [
          Obx(() => Visibility(
                visible: controller.is_user.value == "0" ? true : false,
                child: Container(
                  child: Row(
                    children: [
                      Icon(Icons.add),
                      InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddRestoView(
                                        refresh: controller.getAllRestaurant(
                                            "1")))).then((value) {
                              controller.getAllRestaurant("1");
                            });
                          },
                          child: Center(
                              child: Text(
                            'Resto',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ))),
                      SizedBox(width: 10,)],),),))
        ],
      ),
      body: ListView(
        primary: true,
        shrinkWrap: true,
        children: [
          //search
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              width: MediaQuery.of(context).size.width * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextField(
                cursorColor: Colors.black,
                controller: controller.searchController,
                decoration: InputDecoration(
                  hintText: "Cari",
                  hintStyle: TextStyle(fontSize: 16),
                  suffixIcon: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.search_outlined),
                  ),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
          //category
          Container(
            width: SizeConfig.screenWidth / 1.1,
            height: SizeConfig.screenHight / 10,
            child: Card(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50),
              ),
              elevation: 4,
              child: Container(
                margin: EdgeInsets.only(left: 4, right: 4),
                child: Obx(() => controller.listCategory.length > 0
                    ?  ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: controller.listCategory.length,
                        shrinkWrap: true,
                        primary: false,
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: () {
                              setState(() {
                                controller.current.value = index;
                                controller.idByCurrent.value =
                                    controller.listCategory[index].id!;
                                controller.getAllRestaurant(
                                    controller.idByCurrent.toString());
                                // getPhotofByCategory(idByCurrent);
                              });
                            },
                            child: Center(
                              child: Card(
                                color: controller.current.value == index
                                    ? primary
                                    : secondary,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Text(
                                      "${controller.listCategory[index].name}"),
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : Container()),
              ),
            ),
          ),
          //space
          SizedBox(
            height: 20,
          ),
          //resto
          Obx(() {
            if (controller.listAllRestos.length > 0) {
              return Expanded(
                child: ListView.builder(
                    physics: ClampingScrollPhysics(),
                    shrinkWrap: true,
                    primary: false,
                    itemCount: controller.listAllRestos.length,
                    itemBuilder: (_, index) {
                      return Obx(() => controller.filter.value == null ||
                              controller.filter.value == ""
                          ? Hero(
                              tag: "resto" + index.toString(),
                              child: Material(
                                child: InkWell(
                                  onTap: () {
                                    Get.to(HomeDetailView(
                                        name: controller
                                            .listAllRestos[index].name,
                                        id: controller.listAllRestos[index].id
                                            .toString(),
                                        no: controller.listAllRestos[index].no,
                                        data: controller.listAllRestos[index],
                                        path: controller
                                            .listAllRestos[index].image,
                                        tag: "resto" + index.toString()));
                                  },
                                  child: Stack(children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: NetworkImage(
                                            controller
                                                .listAllRestos[index].image!,
                                          ),
                                        ),
                                      ),
                                      height: 200.0,
                                    ),
                                    Container(
                                      height: 200.0,
                                      width: SizeConfig.screenWidth,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          gradient: LinearGradient(
                                              begin: FractionalOffset.topCenter,
                                              end:
                                                  FractionalOffset.bottomCenter,
                                              colors: [
                                                Colors.transparent,
                                                Colors.white,
                                              ],
                                              stops: [
                                                0.0,
                                                2.0
                                              ])),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  controller
                                                          .listAllRestos[index]
                                                          .name! +
                                                      ", ",
                                                  style:
                                                      TextStyle(fontSize: 25),
                                                ),
                                                Text(
                                                    controller
                                                            .listAllRestos[
                                                                index]
                                                            .address! +
                                                        ", ${controller.listAllRestos[index].meter != null ? controller.listAllRestos[index].meter! > 1000 ? (controller.listAllRestos[index].meter! / 1000).toStringAsFixed(2) : controller.listAllRestos[index].meter!.toStringAsFixed(2) : 0}"
                                                            " ${controller.listAllRestos[index].meter != null ? controller.listAllRestos[index].meter! > 1000 ? 'KM' : 'meters' : 0}",
                                                    style: TextStyle(
                                                        fontSize: 20)),
                                              ],
                                            )),
                                      ),
                                    )
                                  ]),
                                ),
                              ),
                            )
                          : '${controller.listAllRestos[index].name}'
                                  .toLowerCase()
                                  .contains(
                                      controller.filter.value.toLowerCase())
                              ? Hero(
                                  tag: "resto" + index.toString(),
                                  child: Material(
                                    child: InkWell(
                                      onTap: () {
                                        Get.to(HomeDetailView(
                                            name: controller
                                                .listAllRestos[index].name,
                                            id: controller
                                                .listAllRestos[index].id
                                                .toString(),
                                            no: controller
                                                .listAllRestos[index].no
                                                .toString(),
                                            data:
                                                controller.listAllRestos[index],
                                            path: controller
                                                .listAllRestos[index].image,
                                            tag: "resto" + index.toString()));
                                      },
                                      child: Stack(children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.transparent,
                                            image: DecorationImage(
                                              fit: BoxFit.fill,
                                              image: NetworkImage(
                                                controller.listAllRestos[index]
                                                    .image!,
                                              ),
                                            ),
                                          ),
                                          height: 200.0,
                                        ),
                                        Container(
                                          height: 200.0,
                                          width: SizeConfig.screenWidth,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              gradient: LinearGradient(
                                                  begin: FractionalOffset
                                                      .topCenter,
                                                  end: FractionalOffset
                                                      .bottomCenter,
                                                  colors: [
                                                    Colors.transparent,
                                                    Colors.white,
                                                  ],
                                                  stops: [
                                                    0.0,
                                                    2.0
                                                  ])),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Align(
                                                alignment: Alignment.bottomLeft,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      controller
                                                              .listAllRestos[
                                                                  index]
                                                              .name! +
                                                          ", ",
                                                      style: TextStyle(
                                                          fontSize: 25),
                                                    ),
                                                    Text(
                                                        controller
                                                                .listAllRestos[
                                                                    index]
                                                                .address! +
                                                            ", ${controller.listAllRestos[index].meter != null ? controller.listAllRestos[index].meter! > 1000 ? (controller.listAllRestos[index].meter! / 1000).toStringAsFixed(2) : controller.listAllRestos[index].meter!.toStringAsFixed(2) : 0}"
                                                                " ${controller.listAllRestos[index].meter != null ? controller.listAllRestos[index].meter! > 1000 ? 'KM' : 'meters' : 0}",
                                                        style: TextStyle(
                                                            fontSize: 20)),
                                                  ],
                                                )),
                                          ),
                                        )
                                      ]),
                                    ),
                                  ),
                                )
                              : Container());
                    }),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
        ],
      ),
    );
  }
}
