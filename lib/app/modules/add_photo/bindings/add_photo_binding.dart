import 'package:get/get.dart';

import '../controllers/add_photo_controller.dart';

//this file to hang the controller to the view
class AddPhotoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddPhotoController>(
      () => AddPhotoController(),
      fenix: true
    );
  }
}
