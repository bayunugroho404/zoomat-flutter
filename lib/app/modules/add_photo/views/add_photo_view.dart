import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/utils/constant.dart';
import 'package:zoomat/utils/size_config.dart';

import '../controllers/add_photo_controller.dart';

class AddPhotoView extends GetView<AddPhotoController> {
  String? id;
  String? name;
  Future<bool>? refresh;

  AddPhotoView({this.id, this.name,this.refresh});

  final controller = Get.put(AddPhotoController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        title: Text('Add Photo ' + name!),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: GetBuilder<AddPhotoController>(
                  builder: (_){
                    return Column(
                      children: [
                        controller.imageFile != null ?Semantics(
                            child: Image.file(File(controller.imageFile!.path)),
                            label: 'image_picker_example_picked_image'):Text('Image not selected'),
                        ListTile(
                          title: Padding(
                            padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                            child: new RaisedButton(
                              child: Text(
                                  "Choose Image"),
                              color: primary,
                              textColor: Colors.white,
                              onPressed: () {
                                controller.getImageFromGallery();
                              },
                            ),
                          ),
                        ),
                        Visibility(
                          visible: controller.imageFile != null?true:false,
                          child: ListTile(
                            title: Padding(
                              padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                              child: new RaisedButton(
                                child: Text(
                                    "Upload"),
                                color: primary,
                                textColor: Colors.white,
                                onPressed: () {
                                  controller.addPhoto(refresh!,controller.imageFile!, id.toString(),context);
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
