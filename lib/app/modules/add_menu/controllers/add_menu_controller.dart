import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:zoomat/app/data/remote/config/apiServiceImage.dart';
import 'package:zoomat/utils/constant.dart';


//note
// Update(): Using this, widgets can listen to the changes made by the methods defined in controller.
// onInit(): It is called immediately after the widget is allocated memory.
// onReady(): It is called immediately after the widget is rendered on screen.
// onClose(): It is called just before the controller is deleted from memory.
class AddMenuController extends GetxController {
  PickedFile? imageFile;
  final storage = new FlutterSecureStorage();
  dynamic pickImageError;
  final _picker = ImagePicker();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}


  Future getImageFromGallery() async {
    try {
      final pickedFile = await _picker.getImage(
        source: ImageSource.gallery,
      );
      imageFile = pickedFile;
      print(imageFile?.path);
    } catch (e) {
      pickImageError = e;
    }
    update();
  }

  void addMenu(Future<bool> refresh,PickedFile image, String resto_id, BuildContext context)async{
    String token = (await storage.read(key: TOKEN))!;
    ApiServiceImage().addMenu(token, image, resto_id).then((data){
      if(data.message =="File successfully uploaded"){
        Navigator.pop(context);
      }else{
        print(data.message);
      }
    }).catchError((onError){
      print("error" + onError.toString());
    });
  }

}
