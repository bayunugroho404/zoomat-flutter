import 'package:get/get.dart';

import '../controllers/add_menu_controller.dart';
//this file to hang the controller to the view
class AddMenuBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddMenuController>(
      () => AddMenuController(),
    );
  }
}
