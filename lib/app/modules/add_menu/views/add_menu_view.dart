import 'dart:io';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/utils/constant.dart';

import '../controllers/add_menu_controller.dart';

class AddMenuView extends GetView<AddMenuController> {
  String? id;
  String? name;
  Future<bool>? refresh;

  AddMenuView({this.id, this.name,this.refresh});

  final controller = Get.put(AddMenuController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Menu ' + name!),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                clipBehavior: Clip.antiAlias,
                elevation: 0.0,
                color: Colors.grey.shade300,
                child: GetBuilder<AddMenuController>(
                  builder: (_){
                    return Column(
                      children: [
                        controller.imageFile != null ?Semantics(
                            child: Image.file(File(controller.imageFile!.path)),
                            label: 'image_picker_example_picked_image'):Text('Image not selected'),
                        ListTile(
                          title: Padding(
                            padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                            child: new RaisedButton(
                              child: Text(
                                  "Choose Image"),
                              color: primary,
                              textColor: Colors.white,
                              onPressed: () {
                                controller.getImageFromGallery();
                              },
                            ),
                          ),
                        ),
                        Visibility(
                          visible: controller.imageFile != null?true:false,
                          child: ListTile(
                            title: Padding(
                              padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                              child: new RaisedButton(
                                child: Text(
                                    "Upload"),
                                color: primary,
                                textColor: Colors.white,
                                onPressed: () {
                                  controller.addMenu(refresh!,controller.imageFile!, id.toString(),context);
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
