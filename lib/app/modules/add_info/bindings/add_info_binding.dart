import 'package:get/get.dart';

import '../controllers/add_info_controller.dart';


//this file to hang the controller to the view
class AddInfoBinding extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut<AddInfoController>(
      () => AddInfoController(),
    );
  }
}
