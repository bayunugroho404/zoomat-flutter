import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:zoomat/utils/constant.dart';

import '../controllers/add_info_controller.dart';

class AddInfoView extends GetView<AddInfoController> {
  String? id;
  String? name;
  Future<bool>? refresh;

  AddInfoView({this.id, this.name,this.refresh});

  final controller = Get.put(AddInfoController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Info ' + name!),
        backgroundColor: primary,
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: MediaQuery.of(context).size.width * 0.8,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: TextField(
                  cursorColor: Colors.black,
                  controller: controller.infoController,
                  decoration: InputDecoration(
                    hintText: "...",
                    hintStyle: TextStyle(fontSize: 16),
                    suffixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.info_outline),
                    ),
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            ListTile(
              title: Padding(
                padding: EdgeInsets.only(bottom: 00.0, top: 00.0),
                child: new RaisedButton(
                  child: Text(
                      "Submit"),
                  color: primary,
                  textColor: Colors.white,
                  onPressed: () {
                    controller.addInfo(id!, context);
                  },
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
