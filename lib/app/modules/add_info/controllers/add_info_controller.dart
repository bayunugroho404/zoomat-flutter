import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:zoomat/app/data/remote/config/apiService.dart';
import 'package:zoomat/app/modules/widgets/toast.dart';
import 'package:zoomat/utils/constant.dart';

//note
// Update(): Using this, widgets can listen to the changes made by the methods defined in controller.
// onInit(): It is called immediately after the widget is allocated memory.
// onReady(): It is called immediately after the widget is rendered on screen.
// onClose(): It is called just before the controller is deleted from memory.
class AddInfoController extends GetxController {
  TextEditingController infoController = new TextEditingController();
  final storage = new FlutterSecureStorage();

  @override
  void onInit() {
    super.onInit();
  }


  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void addInfo(String resto_id, BuildContext context) async {
    if (infoController.text.isNotEmpty) {
      String token = (await storage.read(key: TOKEN))!;
      APIService().postInfo(infoController.text, resto_id, token).then((data) {
        if (data.message == "input Successfull") {
          infoController.text = "";
          Navigator.pop(context);
        } else {
          print(data.message);
        }
      }).catchError((onError) {
        print("error" + onError.toString());
      });
    } else {
      showToast("data tidak boleh kosong");
    }
  }
}
